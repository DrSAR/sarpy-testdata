Testdata for sarpy
==================

Used as submodule in parent repository sarpy.
The point is to keep a large, mostly binary, rarely changing collection of assets out of the currently quite nimble sarpy repo. 
